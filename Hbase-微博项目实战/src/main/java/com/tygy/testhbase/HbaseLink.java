package com.tygy.testhbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.NamespaceDescriptor.Builder;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author lijiachenggong
 * @version 1.0
 *
 * 1- 连接Hbase
 * 2- 查询Hbase中的某个表是否存在
 * 3- 创建一个表
 * 4- 增，删，改，查
 */

public class HbaseLink {

    static Admin admin = null;
    static Connection connection = null;

    static{
        // 创建Hbase配置对象
        Configuration configuration = HBaseConfiguration.create();

        // Base配置信息
        configuration.set("hbase.zookeeper.quorum", "192.168.100.101");

        try {
            connection = ConnectionFactory.createConnection(configuration);
            // Hbase进程的实例化对象
            admin = connection.getAdmin();
        } catch (IOException e) {
            // TODO: Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws IOException {
//		boolean exit = tableExist("yuangong");
//		boolean exit1 = tableExist("student");
//		System.out.println(exit);
//		System.out.println(exit1);

//		createNameSpace("class");

//		createTable("test123", "info");

//		putData("staff2", "2", "info", "name", "lisi");

//		putBigData("test123");

        scanTable("student");

        // 关闭进程
        close(connection, admin);
    }

    // 查看表是否存在
    public static boolean tableExist(String tableName) throws IOException{

        // 查看表是否存在

        return admin.tableExists(TableName.valueOf(tableName));

    }

    // 创建命名空间
    public static void createNameSpace(String nameSpace) throws IOException{

        if (tableExist(nameSpace)) {
            System.out.println("命名空间已存在");
            return;
        }

        // 创建了一个建造者
        Builder builder = NamespaceDescriptor.create(nameSpace);
        // 创建命名空间
        NamespaceDescriptor build = builder.build();

        admin.createNamespace(build);
        System.out.println("命名空间创建成功");
    }

    // 创建表操作
    public static void createTable(String tableName , String... info) throws IOException{

        if (tableExist(tableName)) {
            System.out.println("表已存在，退出！");
            return;
        }

        // 创建表描述器
        HTableDescriptor hTableDescriptor= new HTableDescriptor(TableName.valueOf(tableName));

        for(String str:info){
            // 创建列族描述器
            HColumnDescriptor hColumnDescriptor = new HColumnDescriptor(str);
            hColumnDescriptor.setMaxVersions(3);
            // 添加列族
            hTableDescriptor.addFamily(hColumnDescriptor);

        }

        // 创建表操作
        admin.createTable(hTableDescriptor);
        System.out.println("创建表成功！");

    }

    // 删除表的操作
    public static void deleteTable(String tableName) throws IOException {

        if (!(tableExist(tableName))) {
            System.out.println("表不存在！");
            return;
        }

        // 删除表的命令
        admin.disableTable(TableName.valueOf(tableName));
        admin.deleteTable(TableName.valueOf(tableName));

        System.out.println("表已经删除！");

    }

    // 导入数据
    public static void putData(String tableName,String rowKey,String cf,String cn,String value) throws IOException {

        // 创建表
        Table table = connection.getTable(TableName.valueOf(tableName));

        // 创建put对象
        Put put = new Put(Bytes.toBytes(rowKey));
        // 导入数据
        put.addColumn(Bytes.toBytes(cf), Bytes.toBytes(cn), Bytes.toBytes(value));

        // 把put对象放入表中
        table.put(put);

        // 打印结果
        System.out.println("数据已经插入成功");

    }

    // 导入大量数据
    public static void putBigData(String tableName) throws IOException {

        // 获取表
        Table table = connection.getTable(TableName.valueOf(tableName));

        // 创建对应的list对象
        ArrayList<Put> arrayList = new ArrayList<>();

        // 导入大量的数据
        for(int i = 0 ; i < 5 ; i++){
            Put put = new Put(Bytes.toBytes("0" + i));
            put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("email"), Bytes.toBytes(i + "@163.com"));
            put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("password"), Bytes.toBytes("1"+ i ));
            arrayList.add(put);
        }


        table.put(arrayList);
        System.out.println("数据已经插入成功！");
    }

    // 删除行数据
    public static void deleteData(String tableName,String rowKey,String cf,String cn) throws IOException {
        // 获取表
        Table table = connection.getTable(TableName.valueOf(tableName));

        // 创建delete对象
        Delete delete = new Delete(Bytes.toBytes(rowKey));
        delete.addColumns(Bytes.toBytes(cf), Bytes.toBytes(cn));
        // 表调用删除方法
        table.delete(delete);

        System.out.println("删除数据成功！");
    }

    // 删除大量
    public static void deleteBigData(String tableName,String rowKey) throws IOException{

        Table table = connection.getTable(TableName.valueOf(tableName));

        ArrayList<Delete> arrayList = new ArrayList<>();
        for (int i = 0; i < 3 ; i++) {
            Delete delete = new Delete(Bytes.toBytes(rowKey+i));
            arrayList.add(delete);
        }

        table.delete(arrayList);
    }

    // 查询数据
    public static void scanTable(String tableName) throws IOException {
        // 获取表
        Table table = connection.getTable(TableName.valueOf(tableName));
        // 创建对应的scan对象
        Scan scan = new Scan();

        ResultScanner results = table.getScanner(scan);
        for (Result result : results) {
            // RowKey的数据
            Cell[] Cells = result.rawCells();
            for (Cell cell : Cells) {
                // 再找列里的数据
                String rk = Bytes.toString(CellUtil.cloneRow(cell));
                String cf = Bytes.toString(CellUtil.cloneFamily(cell));
                String cn = Bytes.toString(CellUtil.cloneQualifier(cell));
                String value = Bytes.toString(CellUtil.cloneValue(cell));

                System.out.println("RowKey: " + rk + ", CF: " + cf +
                        ", CN: " + cn + ", Value: " + value);
            }
        }

        System.out.println("查询数据成功！");

    }

    // 查看指定数据
    public static void getData(String tableName,String rowKey,String cf,String cn) throws IOException{

        // 创建表描述器
        Table table = connection.getTable(TableName.valueOf(tableName));
        // 创建get对象
        Get get = new Get(Bytes.toBytes(rowKey));

        get.addColumn(Bytes.toBytes(cf), Bytes.toBytes(cn));

        // 对表进行get操作
        Result result = table.get(get);
        // 获得 rowkey 数据
        Cell[] Cells = result.rawCells();
        // 循环输出行信息
        for (Cell cell : Cells) {
            String rk = Bytes.toString(CellUtil.cloneRow(cell));
            String cf0 = Bytes.toString(CellUtil.cloneFamily(cell));
            String cn0 = Bytes.toString(CellUtil.cloneQualifier(cell));
            String value = Bytes.toString(CellUtil.cloneValue(cell));

            System.out.println("RowKey: " + rk + ", CF: " + cf0 +
                    ", CN: " + cn0 + ", Value: " + value);
        }
    }

    // 关闭资源
    public static void close(Connection connection,Admin admin){

        if (connection != null) {
            try{
                connection.close();
            }catch(IOException e){
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else if (admin != null) {
            try {
                admin.close();
            } catch (IOException e) {
                // TODO: Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("资源已关闭");

    }

}
