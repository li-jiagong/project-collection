package com.tygy.testdatamapreduce;

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;

/**
 * @author lijiachenggong
 * @version 1.0
 */
public class DataDriver extends Configuration implements Tool {

    private static org.apache.hadoop.conf.Configuration conf = null;

    public static void main(String[] args) throws Exception {
        org.apache.hadoop.conf.Configuration configuration = HBaseConfiguration.create();
        int i = ToolRunner.run(configuration, new DataDriver(), args);
        System.exit(i);
    }

    @Override
    public AppConfigurationEntry[] getAppConfigurationEntry(String name) {
        return new AppConfigurationEntry[0];
    }

    @Override
    public int run(String[] strings) throws Exception {
        // 设置 JOB 对象
//        Job job = Job.getInstance();
        Job job = Job.getInstance(conf);
        // 设置主类
        job.setJarByClass(DataDriver.class);
        // 设置 Mapper
        TableMapReduceUtil.initTableMapperJob("student", new Scan(), DataMapper.class, ImmutableBytesWritable.class, Put.class, job);
        // 设置Reducer
        TableMapReduceUtil.initTableReducerJob("student110", DataReducer.class, job);
        // 提交作业
        boolean result = job.waitForCompletion(true);
        return result ? 0 : 1;
    }

    @Override
    public void setConf(org.apache.hadoop.conf.Configuration configuration) {
        conf = configuration;
    }

    @Override
    public org.apache.hadoop.conf.Configuration getConf() {
        return conf;
    }
}
