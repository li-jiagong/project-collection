package com.tygy.testmapreduce;

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;

import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;


/**
 * @author lijiachenggong
 * @version 1.0
 */
public class TDriver extends Configuration implements Tool {
    /**
     * 1-获取配置文件
     * 2-创建Job对象
     * 3-设置主类（打包时预加载的主类）
     * 4-设置Mapper类
     * 5-设置Reducer类
     * 6-设置输入路径
     * 7-提交作业
     */

    org.apache.hadoop.conf.Configuration conf = null;

    public static void main(String[] args) throws Exception {

        org.apache.hadoop.conf.Configuration configuration = HBaseConfiguration.create();
        int run = ToolRunner.run(configuration, new TDriver(), args);
        System.exit(run);
    }


    @Override
    public AppConfigurationEntry[] getAppConfigurationEntry(String name) {
        return new AppConfigurationEntry[0];
    }

    @Override
    public int run(String[] strings) throws Exception {
        // 创建 job 实例
        Job job = Job.getInstance(conf);

        // 设置主类
        job.setJarByClass(TDriver.class);

        // 设置 Mapper
        job.setMapperClass(TMapper.class);
        job.setMapOutputKeyClass(NullWritable.class);
        job.setMapOutputValueClass(Put.class);

        // 设置 Reducer
        TableMapReduceUtil.initTableReducerJob("student",TReducer.class,job);

        // 设置输入路径
        FileInputFormat.setInputPaths(job,strings[0]);

        //提交作业
        boolean result = job.waitForCompletion(true);

        return result ? 0 : 1;
    }

    @Override
    public void setConf(org.apache.hadoop.conf.Configuration configuration) {
        conf = configuration;
    }

    @Override
    public org.apache.hadoop.conf.Configuration getConf() {
        return null;
    }
}
