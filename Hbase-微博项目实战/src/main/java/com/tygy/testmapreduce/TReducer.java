package com.tygy.testmapreduce;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.io.NullWritable;

import javax.ws.rs.PUT;
import java.io.IOException;

/**
 * @author lijiachenggong
 * @version 1.0
 */
public class TReducer extends TableReducer<NullWritable, Put, NullWritable> {
    @Override
    protected void reduce(NullWritable key, Iterable<Put> values, Context context) throws IOException, InterruptedException {
        // 遍历 map 集合
        for (Put value : values) {
            context.write(NullWritable.get(),value);
        }
    }
}
