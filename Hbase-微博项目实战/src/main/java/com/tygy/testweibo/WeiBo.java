package com.tygy.testweibo;

import java.io.IOException;

/**
 * @author lijiachenggong
 * @version 1.0
 */
public class WeiBo {
    public static void init() throws IOException {
        WeiBoUtil.createNameSpace(Constant.NAMESPACE);
        //创建微博内容表
        WeiBoUtil.createTable(Constant.CONTENT, 1, "info");
        //创建用户关系表
        WeiBoUtil.createTable(Constant.RELATIONS, 1, "attends","fans");
		//创建最新微博内容收件箱表
        WeiBoUtil.createTable(Constant.INBOX, 2, "info");
    }
    public static void main(String[] args) throws IOException {
//        init();
        //发布微博内容（1001、1002发布微博内容）
//        WeiBoUtil.createData("1001", "今天是12-11！！");
//        WeiBoUtil.createData("1002", "今天是12-11@@");
        //关注用户（1001关注1002和1003）
//        WeiBoUtil.addAttend("1001", "1002");
        //查看最新微博内容
//        WeiBoUtil.getData("1001");
        //1003发布微博内容
//        WeiBoUtil.createData("1003", "任务很急");
//        WeiBoUtil.createData("1003", "赶快完成");
//        WeiBoUtil.createData("1003", "时间还有1小时!");
        //查看最新微博内容
//        System.out.println("-----------------------------------------");
        WeiBoUtil.getData("1001");

        //取关用户
//        WeiBoUtil.delAttends("1001", "1002");
//        WeiBoUtil.getData("1001");
        //查看某人微博内容
//        WeiBoUtil.getUserData("1001");
    }
}
