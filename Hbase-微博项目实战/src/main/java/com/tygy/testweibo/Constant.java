package com.tygy.testweibo;

/**
 * @author lijiachenggong
 * @version 1.0
 */
public class Constant {

    // 命名空间
    public static final String NAMESPACE = "weibo";
    // 三张表
    public static final String CONTENT = "weibo:content";
    public static final String RELATIONS = "weibo:relations";
    public static final String INBOX = "weibo:inbox";

}
