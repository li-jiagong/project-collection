"""
@Time : 2023/7/31 19:47
@Author : 憨憨李四
@Email : st124846@163.com
@File : test_main_page.py
@Project : diaoyu_app_test
@feature : 
@实现步骤：
"""
import pytest
from appium.webdriver.common.appiumby import AppiumBy
import pytest, allure, yaml
from common.bases.get_log import logger
from common.bases.get_config import cf


login_data_path = cf.login_data_path


@allure.epic("钓鱼人APP项目自动化测试")
@allure.feature("钓鱼人APP项目自动化测试-账号密码登录功能")
@allure.severity(allure.severity_level.BLOCKER)
class TestLogin:
    """测试登录类"""
    """执行登录测试用例方法"""
    @allure.story("钓鱼人APP项目自动化测试-正向和反向用例")
    @allure.title('{case_name}')
    @pytest.mark.parametrize('case_name, username, password, expected',
                             yaml.safe_load(open(login_data_path, encoding='utf-8')))
    def test_login(self, start_app, case_name, username, password, expected):
        """
        :param start_app:
        :param case_name: 用例名称
        :param username: 账户名
        :param password: 密码
        :param expected: 期望值
        :return:
        """
        lp = start_app
        lp.wait(2)
        lp.open_login_page()
        with allure.step("执行登录操作"):
            allure.attach(f"输入用户名：{username}")
            allure.attach(f"输入密码：{password}")
            allure.attach("点击登录")
        lp.login(username, password)
        if '登录正向用例' in case_name:
            lp.wait(1)
            assert lp.get_username_text() == expected
            logger.info(f"---- 测试用例：{case_name}执行成功！ 用户名：{username} 密码：{password} ----")
        elif '手机号为空' in case_name or '密码为空' in case_name or '账号密码错误' in case_name:
            lp.wait(1)
            assert lp.get_login_title_text() == expected
            lp.to_back()
            lp.wait(1)
            logger.info(f"---- 测试用例：{case_name}执行成功！ 用户名：{username} 密码：{password} ----")
        elif '账号不存在' in case_name:
            lp.wait(1)
            assert lp.get_improve_information_text() == expected
            lp.to_back()
            lp.wait(1)
            logger.info(f"---- 测试用例：{case_name}执行成功！ 用户名：{username} 密码：{password} ----")
        else:
            pass