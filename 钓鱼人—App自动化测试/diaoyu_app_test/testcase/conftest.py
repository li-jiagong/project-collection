"""
@Time : 2023/7/30 22:20
@Author : 憨憨李四
@Email : st124846@163.com
@File : conftest.py
@Project : diaoyu_app_test
@feature : 
@实现步骤：
"""
import logging

# This sample code uses the Appium python client
# pip install Appium-Python-Client
# Then you can paste this into a file and simply run with Python
import  pytest
from appium import webdriver
from pages.login_page import LoginPage


@pytest.fixture(scope='session')
def start_app():

    caps = {}
    caps["platformName"] = "Android"
    caps["platformVersion"] = "9"
    caps["deviceName"] = "127.0.0.1:5554"
    caps["appPackage"] = "com.lchr.diaoyu"
    caps["appActivity"] = "com.lchr.diaoyu.SplashActivity"
    caps["noReset"] = True
    caps["newCommandTimeout"] = 6000
    caps["skipServerInstallation"] = True
    caps["automationName"] = "UiAutomator2"
    caps["ensureWebviewsHavePages"] = True
    caps["appWaitForLaunch"] = False
    caps["enableWebviewDetailsCollection"] = False
    logging.info("钓鱼人APP项目自动化测试开始···")
    driver = webdriver.Remote("http://localhost:4723/wd/hub", caps)
    lp = LoginPage(driver)
    lp.wait(4)
    yield lp

    driver.quit()
    logging.info("钓鱼人APP项目自动化测试结束···")
