"""
@Time : 2023/7/30 22:20
@Author : 憨憨李四
@Email : st124846@163.com
@File : main.py.py
@Project : diaoyu_app_test
@feature : 
@实现步骤：
"""
import pytest,os
from common.bases.get_config import cf
from pages.login_page import LoginPage

json_dir_path = cf.json_dir_path
html_dir_path = cf.html_dir_path

if __name__ == '__main__':
    pytest.main(args=['-s', '-q', '--alluredir', json_dir_path])
    cmd = 'allure generate %s -o %s -c' % (json_dir_path, html_dir_path)
    os.system(cmd)