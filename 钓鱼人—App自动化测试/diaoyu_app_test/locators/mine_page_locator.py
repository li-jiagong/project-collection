"""
@Time : 2023/8/1 10:59
@Author : 憨憨李四
@Email : st124846@163.com
@File : mine_page_locator.py
@Project : diaoyu_app_test
@feature : 
@实现步骤：
"""
from appium.webdriver.common.appiumby import AppiumBy

class MinePageLocator:
    mine_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/btn_tab_mine')
    setting_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/iv_top_navi_setting')
    username_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/user_nick_name')