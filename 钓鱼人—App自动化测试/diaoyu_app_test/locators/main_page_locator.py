"""
@Time : 2023/8/1 10:48
@Author : 憨憨李四
@Email : st124846@163.com
@File : main_page_locator.py
@Project : diaoyu_app_test
@feature : 
@实现步骤：
"""
from appium.webdriver.common.appiumby import AppiumBy

class MainPageLocator:
    home_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/btn_tab_home',)
    search_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/stv_homepage_search')