"""
@Time : 2023/7/20 15:01
@Author : 憨憨李四
@Email : st124846@163.com
@File : login_page_locator.py
@Project : python_workspace
@feature : 
@实现步骤：
"""
from appium.webdriver.common.appiumby import AppiumBy


class LoginPageLocator:
    """定位元素"""
    click_login_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/tv_click2login')
    login_title_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/tv_login_title')
    password_login_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/login_by_pwd')
    username_input_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/et_account')
    password_input_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/et_pwd')
    login_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/btn_login')
    back_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/tb_back')

    improve_information_text_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/tb_title')


