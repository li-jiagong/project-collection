"""
@Time : 2023/8/1 11:33
@Author : 憨憨李四
@Email : st124846@163.com
@File : setting_page_locator.py
@Project : diaoyu_app_test
@feature : 
@实现步骤：
"""
from appium.webdriver.common.appiumby import AppiumBy


class SettingPageLocator:
    quit_loc = (AppiumBy.ID, 'com.lchr.diaoyu:id/rtv_setting_logout')
    true_loc = (AppiumBy.ID, 'android:id/button1')

    # TouchAction(driver).press(x=552, y=1912).move_to(x=521, y=395).release().perform()