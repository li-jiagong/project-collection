"""
@Time : 2023/7/20 20:03
@Author : 憨憨李四
@Email : st124846@163.com
@File : login_page.py
@Project : python_workspace
@feature : 封装登录过程
@实现步骤：
"""

from common.bases.base import Base
from common.bases.get_log import logger
from locators.login_page_locator import LoginPageLocator
from locators.mine_page_locator import MinePageLocator
from locators.setting_page_locator import SettingPageLocator
from appium.webdriver.common.touch_action import TouchAction


class LoginPage(Base):
    """登录类"""
    """打开登录框方法"""

    def open_login_page(self, msg='打开APP登录框'):
        """
        :param msg: 注释说明
        :return:
        # 打开登录框
        """
        # 点击 我的 页面
        self.click_element(MinePageLocator.mine_loc)
        self.wait(1)
        # 判断用户是否登录
        if self.look_username_text():
            self.quit()
        # 点击 登录 和 密码登录
        self.click_element(LoginPageLocator.click_login_loc)
        self.wait(1)
        self.click_element(LoginPageLocator.password_login_loc)
        # 打印登录框信息
        logger.info(msg)

    """关闭登录框方法"""
    def close_login_page(self, msg='关闭登录页面'):
        """
        :param msg: 注释说明
        :return:
        """
        self.click_element(LoginPageLocator.back_loc)
        self.wait(1)
        logger.info(msg)

    """登录方法"""
    def login(self, username, password, msg='登录操作'):
        """
        :param username: 用户名
        :param password: 密码
        :param msg: 注释说明
        :return:
        """
        # 输入数据
        self.send_data(LoginPageLocator.username_input_loc, username)
        self.send_data(LoginPageLocator.password_input_loc, password)
        # 点击协议框
        TouchAction(self.driver).tap(x=74, y=989).perform()
        # 点击登录
        self.click_element(LoginPageLocator.login_loc)
        logger.info(msg + '完成！')

    """退出方法"""

    def quit(self, msg='退出登录成功！'):
        """
        :param msg: 注释说明
        :return:
        """
        # 点击设置按钮
        self.click_element(MinePageLocator.setting_loc)
        self.wait(1)
        # 滑动页面到底部
        self.swipe_page(start_x=480, start_y=1700, duration=200, end_x=500, end_y=400)
        # TouchAction(self.driver).press(x=488, y=1781).wait(200).move_to(x=518, y=405).release().perform()
        # 退出登录
        self.click_element(SettingPageLocator.quit_loc)
        self.wait(1)
        self.click_element(SettingPageLocator.true_loc)
        self.wait(1)
        logger.info(msg + f"")

    """获取用户名"""

    def get_username_text(self, msg='获取用户名:'):
        """
        :param msg: 注释说明
        :return: 用户名
        """
        username_text = self.get_element_text(MinePageLocator.username_loc)
        logger.info(msg + f"{username_text}")
        return username_text

    """检验是否有用户名"""

    def look_username_text(self, msg='检验是否有用户名:'):
        """
        :param msg: 注释说明
        :return: True or False
        """
        username_text = self.get_element_text(MinePageLocator.username_loc)
        logger.info(msg + f"{username_text}")
        if username_text:
            return True
        else:
            return False

    """
    检验用户名或密码或协议框为空
    或
    密码错误方法
    """

    def get_login_title_text(self, msg='获取登录主题文字:'):
        """
        :param msg: 注释说明
        :return: 密码登录
        """
        login_title_text = self.get_element_text(LoginPageLocator.login_title_loc)
        logger.info(msg + f"{login_title_text}")
        return login_title_text

    """检验用户名不存在方法"""

    def get_improve_information_text(self, msg='获取完善资料文字:'):
        """
        :param msg: 注释说明
        :return: 完善资料
        """
        improve_information_text = self.get_element_text(LoginPageLocator.improve_information_text_loc)
        logger.info(msg + f"{improve_information_text}")
        return improve_information_text

    """返回上级页面方法"""
    def to_back(self,msg='返回上级页面'):
        """
        :param msg: 注释说明
        :return:
        """
        self.driver.back()
        logger.info(msg)