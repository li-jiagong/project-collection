"""
@Time : 2023/8/5 12:07
@Author : 憨憨李四
@Email : st124846@163.com
@File : course.py
@Project : KDTX
@feature : 
@实现步骤：
"""
import requests
from url.course_urls import course_urls

class CourseApi:
    """接口API类"""
    """初始化"""
    def __init__(self):
        self.add_course_url = course_urls.add_course_url
        self.search_course_url = course_urls.search_course_url
        self.alert_course_url = course_urls.alert_course_url
        self.delete_course_url = course_urls.delete_course_url

    """添加课程"""
    def add_course(self, test_data, token):
        """
        :param test_data: Json数据
        :param token: 登录标识
        :return: 响应体对象
        """
        headers ={
            "Authorization": token
        }
        return requests.post(url=self.add_course_url, headers=headers, json=test_data)

    """查询课程"""
    def search_course(self, test_data, token):
        """
        :param test_data: Json 数据
        :param token: 登录标识
        :return: 响应体对象
        """
        headers = {
            "Authorization": token
        }
        return requests.get(url=self.search_course_url, headers=headers, json=test_data)

    """修改课程"""
    def alert_course(self, test_data, token):
        """
        :param test_data: Json 数据
        :param token: 登录标识
        :return: 响应体对象
        """
        headers = {
            "Authorization": token
        }
        return requests.put(url=self.alert_course_url, headers=headers, json=test_data)

    """删除课程"""
    def delete_course(self, course_id, token):
        """
        :param course_id: 要删除课程的 ID
        :param token: 登录标识
        :return: 响应体对象
        """
        headers = {
            "Authorization": token
        }
        return requests.delete(url=self.delete_course_url + f'/{course_id}', headers=headers)


courseApi = CourseApi()