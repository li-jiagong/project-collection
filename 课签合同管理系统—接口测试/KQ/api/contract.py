"""
@Time : 2023/8/5 12:17
@Author : 憨憨李四
@Email : st124846@163.com
@File : contract.py
@Project : KDTX
@feature : 
@实现步骤：
"""
import requests
from url.contract_urls import contract_urls

class ContractApi:
    """接口API类"""
    """初始化"""
    def __init__(self):
        self.upload_contract_url = contract_urls.upload_contract_url
        self.add_contract_url = contract_urls.add_contract_url
        self.search_contract_url = contract_urls.search_contract_url
        self.delete_contract_url = contract_urls.delete_contract_url

    """上传合同"""
    def upload_contract(self, test_data, token):
        """
        :param test_data: File文件
        :param token: 登录标识
        :return: 响应体对象
        """
        headers = {
            "Authorization": token
        }
        files = {
            "file": test_data
        }
        return requests.post(url=self.upload_contract_url, headers=headers, files=files)

    """添加合同"""
    def add_contract(self, test_data, token):
        """
        :param token: 登录标识
        :param test_data: File文件
        :return:
        """
        headers = {
            "Authorization": token
        }
        return requests.post(url=self.add_contract_url, headers=headers, json=test_data)

    """查询合同"""
    def search_contract(self):
        """
        :return:
        """
        return requests.get(url=self.search_contract_url)

    """删除合同"""
    def delete_contract(self):
        """
        :return:
        """
        return requests.get(url=self.delete_contract_url)


contractApi = ContractApi()