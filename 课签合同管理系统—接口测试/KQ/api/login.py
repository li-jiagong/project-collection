"""
@Time : 2023/8/5 10:58
@Author : 憨憨李四
@Email : st124846@163.com
@File : login.py
@Project : KDTX
@feature : 
@实现步骤： 封装登录信息
"""
import requests
from url.login_urls import login_urls

class LoginApi:
    """接口API类"""
    """初始化"""
    def __init__(self):
        self.code_url = login_urls.code_url
        self.login_url = login_urls.login_url

    """获取验证码"""
    def get_code(self):
        """
        :return: 响应体对象
        """
        return requests.get(url=self.code_url)

    """登录"""
    def login(self, test_data):
        """
        :param header_data: 请求头数据
        :param test_data: 测试 json 数据
        :return: 响应体对象
        """
        header_data = {
            "Content-Type": "application/json"
        }
        return requests.post(url=self.login_url, headers=header_data, json=test_data)


loginApi = LoginApi()