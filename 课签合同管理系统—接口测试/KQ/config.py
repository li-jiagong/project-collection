"""
@Time : 2023/8/6 11:42
@Author : 憨憨李四
@Email : st124846@163.com
@File : config.py
@Project : KDTX
@feature : 配置文件
@实现步骤：
"""
# 导包
import os

# 设置项目环境域名
BASE_URL = "http://kdtx-test.itheima.net"

# 获取项目根路径
BASE_DIR = os.path.dirname(__file__)
# if ":" in BASE_DIR:
#     BASE_DIR = BASE_DIR.replace('/', '\\')

json_dir_path = BASE_DIR + "/report/json"
html_dir_path = BASE_DIR + "/report/html"
