"""
@Time : 2023/8/6 11:24
@Author : 憨憨李四
@Email : st124846@163.com
@File : test06_delete_course.py
@Project : KDTX
@feature : 
@实现步骤：
"""
import pytest

from api.course import courseApi
from api.login import loginApi

"""测试用例01：登录成功"""
@pytest.fixture(scope="function")
def test01_login_success():
    """
    :return: token
    """
    # 获取验证码
    res_c = loginApi.get_code()
    # 提取 uuid 值
    uuid = res_c.json().get('uuid')
    # 登录数据
    data = {
        "username": "admin",
        "password": "HM_2023_test",
        "code": 2,
        "uuid": uuid
    }
    # 发送请求
    response = loginApi.login(test_data=data)
    # 提取 token 值
    return response.json().get('token')

class TestDeleteCourse:
    """测试添加课程类"""

    """测试用例02：修改课程成功(ID存在)"""
    def test02_delete_course_success(self, test01_login_success):
        # 发送请求
        response = courseApi.delete_course(course_id=92, token=test01_login_success)
        # 查看响应
        print(response.json())
        # 断言响应状态码
        assert 200 == response.status_code
        # 断言响应数据包含“成功”
        assert "成功" in response.text
        # 断言响应Json数据中code值
        assert "操作成功" == response.json().get('msg')

    """测试用例03：修改课程成功(ID为空)"""
    def test03_delete_course_fail_id_none(self, test01_login_success):
        # 发送请求
        response = courseApi.delete_course(course_id=None, token=test01_login_success)
        # 查看响应
        print(response.json())
        # 断言响应状态码
        assert 200 == response.status_code
        # 断言响应数据包含“成功”
        assert "失败" in response.text
        # 断言响应Json数据中code值
        assert "操作失败" == response.json().get('msg')

    """测试用例04：修改课程成功(ID非数字)"""
    def test04_delete_course_fail_id_english(self, test01_login_success):
        # 发送请求
        response = courseApi.delete_course(course_id="None", token=test01_login_success)
        # 查看响应
        print(response.json())
        # 断言响应状态码
        assert 200 == response.status_code
        # 断言响应数据包含“成功”
        assert "失败" in response.text
        # 断言响应Json数据中code值
        assert "操作失败" == response.json().get('msg')

    """测试用例05：修改课程成功(ID为不存在)"""
    def test05_delete_course_fail_id_not_exist(self, test01_login_success):
        # 发送请求
        response = courseApi.delete_course(course_id=92452, token=test01_login_success)
        # 查看响应
        print(response.json())
        # 断言响应状态码
        assert 200 == response.status_code
        # 断言响应数据包含“成功”
        assert "失败" in response.text
        # 断言响应Json数据中code值
        assert "操作失败" == response.json().get('msg')

    """测试用例06：修改课程成功(未登录)"""
    def test06_delete_course_fail_not_login(self):
        # 发送请求
        response = courseApi.delete_course(course_id=92452, token=None)
        # 查看响应
        print(response.json())
        # 断言响应状态码
        assert 200 == response.status_code
        # 断言响应数据包含“成功”
        assert "失败" in response.text
        # 断言响应Json数据中code值
        assert "请求访问：/clues/course/92452，认证失败，无法访问系统资源" == response.json().get('msg')