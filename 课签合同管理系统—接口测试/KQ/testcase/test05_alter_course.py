"""
@Time : 2023/8/5 13:11
@Author : 憨憨李四
@Email : st124846@163.com
@File : test03_add_course.py
@Project : KDTX
@feature : 
@实现步骤：
"""
import json

import pytest
from api.login import loginApi
from api.course import courseApi
from common.base.get_config import cf

course_alter_path = cf.course_alter_path

# 读取 Json 文件
def build_data(json_file):
    # 定义空列表
    test_data = []
    # 打开 Json 文件
    with open(json_file, "r",encoding="utf-8") as f:
        json_data = json.load(f)
        for case_data in json_data:
            # 转换格式[{},{}] -> [(),()]
            id = case_data.get('id')
            name = case_data.get('name')
            subject = case_data.get('subject')
            price = case_data.get('price')
            applicablePerson = case_data.get('applicablePerson')
            info = case_data.get('info')
            status = case_data.get('status')
            message = case_data.get('message')
            msg = case_data.get('msg')
            test_data.append((id, name, subject, price, applicablePerson, info, status, message, msg))
    # 返回处理之后的数据
    return test_data

"""测试用例01：登录成功"""
@pytest.fixture(scope="function")
def test01_login_success():
    """
    :return: token
    """
    # 获取验证码
    res_c = loginApi.get_code()
    # 提取 uuid 值
    uuid = res_c.json().get('uuid')
    # 登录数据
    data = {
        "username": "admin",
        "password": "HM_2023_test",
        "code": 2,
        "uuid": uuid
    }
    # 发送请求
    response = loginApi.login(test_data=data)
    # 提取 token 值
    return response.json().get('token')

class TestAlterCourse:
    """测试添加课程类"""

    """测试用例02：修改课程"""
    @pytest.mark.parametrize("id, name, subject, price, applicablePerson, info, status, message, msg", build_data(course_alter_path))
    def test02_alter_course(self, test01_login_success, id, name, subject, price, applicablePerson, info, status, message, msg):
        add_data = {
            "id": id,
            "name": name,
            "subject": subject,
            "price": price,
            "applicablePerson": applicablePerson,
            "info": info
        }
        # 发送请求
        response = courseApi.alert_course(test_data=add_data, token=test01_login_success)
        # 查看响应
        print(response.json())
        # 断言响应状态码
        assert status == response.status_code
        # 断言响应数据包含“成功”
        assert message in response.text
        # 断言响应Json数据中code值
        assert msg == response.json().get('msg')
