"""
@Time : 2023/8/5 12:43
@Author : 憨憨李四
@Email : st124846@163.com
@File : test01_image.py
@Project : KDTX
@feature : 
@实现步骤：
"""
from api.login import loginApi

"""测试用例01：获取验证码"""
def test01_image():
    # 发送请求
    response = loginApi.get_code()
    # 查看响应
    print(response.status_code)
    print(response.text)