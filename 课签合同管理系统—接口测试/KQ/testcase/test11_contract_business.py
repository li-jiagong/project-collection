"""
@Time : 2023/8/5 22:43
@Author : 憨憨李四
@Email : st124846@163.com
@File : test11_contract_business.py
@Project : KDTX
@feature : 
@实现步骤：
"""
import config
from api.course import courseApi
from api.login import loginApi
from api.contract import contractApi

class TestContractBusiness:
    """测试添加课程类"""
    token = None
    filename = None

    """测试用例01：登录成功"""
    def test01_login_success(self):
        """
        :return: token
        """
        # 获取验证码
        res_c = loginApi.get_code()
        # 提取 uuid 值
        uuid = res_c.json().get('uuid')
        # 登录数据
        data = {
            "username": "admin",
            "password": "HM_2023_test",
            "code": 2,
            "uuid": uuid
        }
        # 发送请求
        response = loginApi.login(test_data=data)
        # 提取 token 值
        TestContractBusiness.token = response.json().get('token')
        print(TestContractBusiness.token)

    """测试用例02：添加课程"""

    def test02_add_course(self):
        add_data = {
            "name": "测试开发提升课",
            "subject": "6",
            "price": "899",
            "applicablePerson": "2",
            "info": "测试开发提升课",
        }
        response = courseApi.add_course(test_data=add_data, token=TestContractBusiness.token)
        print(response.json())

    """测试用例03：上传合同"""

    def test03_upload_contract(self):
        f = open(config.BASE_DIR + "/data/ohb.pdf", "rb")
        response = contractApi.upload_contract(test_data=f, token=TestContractBusiness.token)
        TestContractBusiness.filename = response.json().get('filename')
        print(response.json())

    """测试用例04：新增合同"""
    def test04_add_contract(self):
        add_data = {
            "name": "测试888",
            "phone": "13835033488",
            "contractNo": "HT10875559",
            "subject": "6",
            "courseId": 99,
            "channel": "0",
            "activityId": 77,
            "fileName": TestContractBusiness.filename
        }
        response = contractApi.add_contract(add_data, TestContractBusiness.token)
        print(response.json())