"""
@Time : 2023/8/5 12:40
@Author : 憨憨李四
@Email : st124846@163.com
@File : test02_login.py
@Project : KDTX
@feature : 
@实现步骤：
"""
import pytest, json
from api.login import loginApi
from common.base.get_config import cf

login_data_path = cf.login_data_path

# 读取 Json 文件
def build_data(json_file):
    # 定义空列表
    test_data = []
    # 打开 Json 文件
    with open(json_file, "r",encoding="utf-8") as f:
        json_data = json.load(f)
        for case_data in json_data:
            username = case_data.get('username')
            password = case_data.get('password')
            code = case_data.get('code')
            status = case_data.get('status')
            message = case_data.get('message')
            msg = case_data.get('msg')
            test_data.append((username, password, code, status, message, msg))
    # 返回处理之后的数据
    return test_data

"""测试用例01：获取验证码"""
@pytest.fixture(scope="function")
def test01_image():
    # 发送请求
    response = loginApi.get_code()
    # 查看响应
    print(response.json())
    # 提取 token 值
    return response.json().get('uuid')

class TestLogin:
    """测试登录类"""

    """测试用例02：登录"""
    @pytest.mark.parametrize("username, password, code, status, message, msg", build_data(login_data_path))
    def test02_login(self, test01_image, username, password, code, status, message, msg):
        # 登录数据
        login_data = {
            "username": username,
            "password": password,
            "code": code,
            "uuid": test01_image
        }
        # 发送请求
        response = loginApi.login(test_data=login_data)
        # 查看响应
        print(response.json())
        # 断言响应状态码
        assert status == response.status_code
        # 断言响应数据包含“成功”
        assert message in response.text
        # 断言响应Json数据中code值
        assert msg == response.json().get('msg')
