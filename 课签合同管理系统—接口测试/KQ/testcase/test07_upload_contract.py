"""
@Time : 2023/8/5 13:09
@Author : 憨憨李四
@Email : st124846@163.com
@File : test07_upload_contract.py
@Project : KDTX
@feature : 
@实现步骤：
"""
import config
from api.login import loginApi
from api.contract import contractApi

class TestUploadContract:
    """测试添加课程类"""
    token = None

    """测试用例01：登录成功"""
    def test01_login_success(self):
        """
        :return: token
        """
        # 获取验证码
        res_c = loginApi.get_code()
        # 提取 uuid 值
        uuid = res_c.json().get('uuid')
        # 登录数据
        data = {
            "username": "admin",
            "password": "HM_2023_test",
            "code": 2,
            "uuid": uuid
        }
        # 发送请求
        response = loginApi.login(test_data=data)
        # 提取 token 值
        TestUploadContract.token = response.json().get('token')
        print(TestUploadContract.token)

    """测试用例02：上传合同"""
    def test02_upload_contract(self):
        f = open(config.BASE_DIR + "/data/ohb.pdf", "rb")
        response = contractApi.upload_contract(test_data=f, token=TestUploadContract.token)
        print(response.json())