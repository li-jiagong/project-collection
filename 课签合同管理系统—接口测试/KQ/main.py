# 导包
import pytest, os
import config

json_dir_path = config.json_dir_path
html_dir_path = config.html_dir_path

# 按间距中的绿色按钮以运行脚本。
if __name__ == '__main__':
    pytest.main(args=['-s', '-q', '--alluredir', json_dir_path])
    cmd = 'allure generate %s -o %s -c' % (json_dir_path ,html_dir_path)
    os.system(cmd)


