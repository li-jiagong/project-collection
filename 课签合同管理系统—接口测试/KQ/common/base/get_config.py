"""
@Time : 2023/7/20 12:04
@Author : 憨憨李四
@Email : st124846@163.com
@File : get_config.py.py
@Project : python_workspace
@feature : 
@实现步骤：
"""
import os
import configparser
from common.base.get_log import logger

# 文件路径
BASE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
config_file_path = os.path.abspath(os.path.join(BASE_DIR, 'common/config/config.ini'))


class Config(object):
    """配置类"""
    """初始化Config类"""
    def __init__(self):
        self.cf = configparser.ConfigParser()
        self.json_dir_path = self.read_config('reports', 'json_dir_path')
        self.html_dir_path = self.read_config('reports', 'html_dir_path')
        self.screenshots_dir_path = self.read_config('screenshots', 'screenshots_dir_path')
        self.login_data_path = self.read_config('datas', 'login_data_path')
        self.course_add_path = self.read_config('datas', 'course_add_path')
        self.course_search_path = self.read_config('datas', 'course_search_path')
        self.course_alter_path = self.read_config('datas', 'course_alter_path')
        self.course_del_path = self.read_config('datas', 'course_del_path')

    """读取配置文件"""
    def read_config(self, field, key):
        try:
            self.cf.read(config_file_path, encoding='utf-8')
            path =self.cf.get(field, key).replace('BASE_DIR', BASE_DIR)
            if ":" in path:
                pa = path.replace('/', '\\')
            return path
        except Exception as e:
            logger.error(e)

# 实例化配置类对象
cf = Config()


if __name__ == '__main__':
    print(cf.json_dir_path)
    print(cf.html_dir_path)
    print(cf.course_search_path)