"""
@Time : 2023/8/5 12:05
@Author : 憨憨李四
@Email : st124846@163.com
@File : contract_urls.py
@Project : KDTX
@feature : 
@实现步骤：
"""
from url.common_url import commonUrl


class ContractUrls:
    upload_contract_url = commonUrl.common_url + "/api/common/upload"
    add_contract_url = commonUrl.common_url + "/api/contract"
    search_contract_url = commonUrl.common_url + "/api/contract/list"
    delete_contract_url = commonUrl.common_url + "/api/contract/remove"


contract_urls = ContractUrls()