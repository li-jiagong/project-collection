"""
@Time : 2023/8/5 12:02
@Author : 憨憨李四
@Email : st124846@163.com
@File : course_urls.py
@Project : KDTX
@feature : 
@实现步骤：
"""
from url.common_url import commonUrl


class CourseUrls:
    add_course_url = commonUrl.common_url + "/api/clues/course"
    search_course_url = commonUrl.common_url + "/api/clues/course/list"
    alert_course_url = commonUrl.common_url + "/api/clues/course/"
    delete_course_url = commonUrl.common_url + "/api/clues/course"


course_urls = CourseUrls()
