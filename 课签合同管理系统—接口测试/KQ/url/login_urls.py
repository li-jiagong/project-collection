"""
@Time : 2023/8/5 11:06
@Author : 憨憨李四
@Email : st124846@163.com
@File : login_urls.py
@Project : KDTX
@feature : 
@实现步骤： 封装接口URL
"""
from url.common_url import commonUrl


class LoginUrls:
    code_url = commonUrl.common_url + "/api/captchaImage"
    login_url = commonUrl.common_url + "/api/login"


login_urls = LoginUrls()
