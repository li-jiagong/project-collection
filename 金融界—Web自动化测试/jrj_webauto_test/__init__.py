"""
@Time : 2023/7/20 12:10
@Author : 憨憨李四
@Email : st124846@163.com
@File : get_config.py
@Project : python_workspace
@feature : 
@实现步骤：
1. 基于po思想，采用分层设计创建项目目录结构
2. 在data目录下， login_data.yaml文件中创建登录测试用例数据
3. 添加日志功能
4. 处理配置文件数据
5. 编写页面元素定位
6. 在pages文件夹中，封装网站登录功能
7. 在testcase文件夹中，编写浏览器启动初始化
8. 在testcase文件夹中，编写测试用例
"""
import pytest,os
from jrj_webauto_test.common.get_config import cf
from jrj_webauto_test.pages.login_page import LoginPage

json_dir_path = cf.json_dir_path
html_dir_path = cf.html_dir_path


if __name__ == '__main__':
    pytest.main(args=['-s', '-q', '--alluredir', json_dir_path])
    cmd = 'allure generate %s -o %s -c' % (json_dir_path ,html_dir_path)
    os.system(cmd)