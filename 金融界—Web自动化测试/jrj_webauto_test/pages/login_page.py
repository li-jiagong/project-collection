"""
@Time : 2023/7/20 20:03
@Author : 憨憨李四
@Email : st124846@163.com
@File : login_page.py
@Project : python_workspace
@feature : 封装登录过程
@实现步骤：
"""
from jrj_webauto_test.common.base import Base
from jrj_webauto_test.common.get_log import logger
from jrj_webauto_test.locators.login_locator import LoginLocator


class LoginPage(Base):
    """登录类"""
    """打开登录框方法"""

    def show_login_dialog_window(self, url, msg='打开登录框'):
        """
        :param url: 网址
        :param msg: 注释说明
        :return:
        """
        # 打开浏览器
        self.open_test_web(url)
        self.click_element(LoginLocator.login_register_loc)
        self.click_element(LoginLocator.account_password_login_loc)
        # 打印登录框信息
        logger.info(msg)

    """登录方法"""

    def login(self, username, password, msg='登录操作！'):
        """
        :param username: 用户名
        :param password: 密码
        :param msg: 注释说明
        :return:
        """
        # 输入数据
        self.send_data(LoginLocator.username_input_loc, username)
        self.send_data(LoginLocator.password_input_loc, password)
        # 点击登录
        self.click_element(LoginLocator.login_loc)
        logger.info(msg + '完成！')
        # self.wait(4)
        # assert self.search_element(LoginLocator.username_loc)

    """获取用户名方法"""

    def get_username_text(self, msg='获取用户名:'):
        """
        :param msg: 注释说明
        :return:
        """
        username_text = self.get_element_text(LoginLocator.username_loc)
        logger.info(msg + f'{username_text}')
        return username_text

    """获取请输入用户名/手机号提示方法"""

    def get_username_empty_text(self, msg='获取请输入用户名/手机号提示'):
        """
        :param msg: 注释说明
        :return:
        """
        username_empty_text = self.get_element_text(LoginLocator.username_error_lco)
        logger.info(msg + "username_error_lco")
        return username_empty_text

    """获取请输入密码提示方法"""

    def get_password_empty_text(self, msg='获取请输入密码提示'):
        """
        :param msg: 注释说明
        :return:
        """
        password_empty_text = self.get_element_text(LoginLocator.password_error_lco)
        logger.info(msg + "password_error_lco")
        return password_empty_text

    """获取用户名或密码错误提示语方法"""

    def get_username_password_error_text(self, msg='获取用户名或密码错误提示语'):
        """
        :param msg: 注释说明
        :return:
        """
        username_password_error_text = self.get_element_text(LoginLocator.account_password_error_loc)
        logger.info(msg + "account_password_error_loc")
        return username_password_error_text
