"""
@Time : 2023/7/20 14:12
@Author : 憨憨李四
@Email : st124846@163.com
@File : get_log.py
@Project : python_workspace
@feature : 
@实现步骤：
"""
import os
import logging


project_name = os.path.basename(os.path.dirname(os.path.dirname(__file__)))


def get_log():
    logging.basicConfig(level='DEBUG', format='%(name)s - %(levelname)s - %(asctime)s - %(filename)s '
                                              '- [lineNo: %(lineno)d]: %(message)s')
    log = logging.getLogger(project_name)
    return log

logger = get_log()

if __name__ == '__main__':
    logger.debug('this is debug msg.')
    logger.info('this is info msg.')
    logger.warning('this is warn msg.')
    logger.error('this is error msg.')
    logger.critical('this is critical msg.')
