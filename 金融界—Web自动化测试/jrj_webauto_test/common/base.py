"""
@Time : 2023/7/20 16:46
@Author : 憨憨李四
@Email : st124846@163.com
@File : base.py
@Project : python_workspace
@feature : 页面元素操作方法的封装。例如：查找元素，点击元素
@实现步骤：
"""
import os.path
from time import sleep, strftime
from jrj_webauto_test.common.get_log import logger
from jrj_webauto_test.common.get_config import cf
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

screenshots_dir_path = cf.screenshots_dir_path


class Base:
    """基础类"""
    def __init__(self, driver):
        self.driver = driver

    """等待方法"""
    def wait(self, s, msg="等待："):
        """
        :param s: 等待时间，单位：s
        :param msg: 注释说明
        :return:
        """
        sleep(s)
        logger.info(msg + f"{s} seconds.")

    """打开测试网页方法"""
    def open_test_web(self, url, msg='打开测试页面：'):
        """
        :param url: 打开的网址
        :param msg: 注释说明
        :return:
        """
        self.driver.delete_all_cookies()
        self.driver.get(url)
        logger.info(msg + f"{url}")

    """搜索网页元素方法"""
    def search_element(self, loc, msg='找到页面元素：'):
        """
        :param loc: 元素定位
        :param msg: 注释说明
        :return: element
        """
        try:
            logger.info(f"开始查找页面元素： {loc}")
            # 创建 Wait 对象
            # wait = WebDriverWait(self.driver, 2, 0.3)
            # wait.until(EC.visibility_of(self.driver.find_element(*loc)))
            element = self.driver.find_element(*loc)
            logger.info(msg + f"{loc}")
            return element
        except Exception as e:
            logger.warning(e)
            self.sava_pic()

    """点击网页元素方法"""
    def click_element(self, loc, msg='点击页面元素：'):
        """
        :param loc: 元素定位
        :param msg: 注释说明
        :return:
        """
        self.search_element(loc).click()
        logger.info((msg + f"{loc}"))

    """输入数据方法"""
    def send_data(self, loc, data, msg='给页面元素：'):
        """
        :param loc: 元素定位
        :param data: 数据
        :param msg: 注释说明
        :return:
        """
        self.search_element(loc).send_keys(data)
        logger.info(msg + f"{loc} 发送数据 {data}")

    """获取元素文本方法"""
    def get_element_text(self, loc, msg='获取页面元素文本：'):
        """
        :param loc: 元素定位
        :param msg: 注释说明
        :return: 页面元素文本
        """
        text = self.search_element(loc).text
        logger.info(msg + f"{text}")
        return text

    """截图方法"""
    def sava_pic(self, msg='snapshot'):
        filename = screenshots_dir_path + '{0} - {1}.png'.format(msg, strftime('%Y_%m_%d_%H_%M_%S'))
        try:
            if not  os.path.exists(filename):
                self.driver.save_screenshot(filename)
                logger.info(f"截图成功，图片保存路径为：{filename}")
            else:
                logger.info("截图失败！")
        except Exception as e:
            logger.warning(e)