"""
@Time : 2023/7/20 22:17
@Author : 憨憨李四
@Email : st124846@163.com
@File : conftest.py
@Project : python_workspace
@feature : 实现执行测试用例前的打开浏览器工作，既测试后收尾工作
@实现步骤：
"""
import logging
import pytest
from selenium import webdriver
from jrj_webauto_test.common.get_log import logger
from jrj_webauto_test.pages.login_page import LoginPage
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


"""打开浏览的的初始化方法"""
@pytest.fixture(scope='session')
def open_bowser_quit():
    logging.info('打开chrome浏览器---------')
    service = Service(executable_path=ChromeDriverManager().install())
    option = webdriver.ChromeOptions()
    option.add_argument('--start-maximized')
    # 消除浏览器自动保存密码操作 和 去掉浏览器正在被自动化调用
    prefs = {"credentials_enable_service": False, "profile.password_manager_enabled": False}
    option.add_experimental_option("prefs", prefs)
    option.add_experimental_option('excludeSwitches', ['enable-automation'])
    # 创建浏览器驱动对象
    driver = webdriver.Chrome(options=option, service=service)
    # 创建登录模块对象
    lp = LoginPage(driver)
    yield lp
    driver.quit()
    logger.info("金融界WEB网站测试用例全部执行完毕！")
    logger.info("关闭浏览器---------")