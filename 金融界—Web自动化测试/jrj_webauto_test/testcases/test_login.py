"""
@Time : 2023/7/21 13:43
@Author : 憨憨李四
@Email : st124846@163.com
@File : test_login.py
@Project : python_workspace
@feature : 执行网站登录功能的测试正反用例
@实现步骤：
"""
import pytest, allure, yaml
from jrj_webauto_test.common.get_log import logger
from jrj_webauto_test.common.get_config import cf


login_data_path = cf.login_data_path


@allure.epic("金融界WEB项目自动化测试")
@allure.feature("金融界WEB自动化测试-账号密码登录功能")
class TestLogin:
    """测试登录类"""
    """执行登录测试用例方法"""
    @allure.story("金融界WEB自动化测试-正向和反向用例")
    @allure.title('{case_name}')
    @pytest.mark.parametrize('case_name, url, username, password, expected',
                             yaml.safe_load(open(login_data_path, encoding='utf-8')))
    def test_login(self, open_bowser_quit, case_name, url, username, password, expected):
        login_pages = open_bowser_quit
        login_pages.show_login_dialog_window(url)
        login_pages.login(username, password)
        login_pages.wait(1)
        if '登录正向用例' in case_name:
            assert login_pages.get_username_text() == expected
            logger.info(f"---- 测试用例：{case_name}执行成功！ 用户名：{username} 密码：{password} ----")
        elif '手机号为空' in case_name:
            assert login_pages.get_username_empty_text() == expected
            logger.info(f"---- 测试用例：{case_name}执行成功！ 用户名：{username} 密码：{password} ----")
        elif '密码为空' in case_name:
            assert login_pages.get_password_empty_text() == expected
            logger.info(f"--- 0- 测试用例：{case_name}执行成功！ 用户名：{username} 密码：{password} ----")
        elif '账号不存在或账号密码错误' in case_name:
            assert login_pages.get_username_password_error_text() == expected
            logger.info(f"---- 测试用例：{case_name}执行成功！ 用户名：{username} 密码：{password} ----")

