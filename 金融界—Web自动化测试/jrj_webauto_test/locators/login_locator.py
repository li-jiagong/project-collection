"""
@Time : 2023/7/20 15:01
@Author : 憨憨李四
@Email : st124846@163.com
@File : login_locator.py
@Project : python_workspace
@feature : 
@实现步骤：
"""
from selenium.webdriver.common.by import By


class LoginLocator:
    login_register_loc = (By.ID, 'noLogin',)
    account_password_login_loc = (By.ID, 'logOdNumIn')
    username_input_loc = (By.ID, 'inputUser')
    password_input_loc = (By.ID, 'inputPswd')

    username_error_lco = (By.XPATH, '//div[@id="acountRequired"]/div')
    password_error_lco = (By.XPATH, '//div[@id="pswdRequired"]/div')
    account_password_error_loc = (By.XPATH, '//div[@id="pswdRequired"]/div')

    username_loc = (By.CSS_SELECTOR, '#userInfoView > span')
    login_loc = (By.ID, 'submitLog')


